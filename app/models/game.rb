class Game < ActiveResource::Base
  
  self.site = "http://localhost:2000"
  
  schema do
      attribute 'gamer_id', 'integer'
      attribute 'score', 'integer'
    end
    
    validates_presence_of :gamer_id, :score
  
end
