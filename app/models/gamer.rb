class Gamer < ActiveRecord::Base
  
  validates_presence_of :name
  validates_uniqueness_of :name, :email, :case_sensitive => false
  
  # Include default devise modules. Others available are:
  # :token_authenticatable, :encryptable, :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :name, :email, :password, :password_confirmation, :remember_me
  # attr_accessible :title, :body
  
  def games
    Game.find(:all, :from => :find_by_gamer_id, :params =>{:gamer_id => self.id})
  end
  
  def top_ten_scores
    games.sort(&:score).reverse.slice!(0..9)
  end
  
end
