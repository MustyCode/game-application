class HomeController < ApplicationController
  def index
    @gamers = Gamer.all
    if !Game.all.empty?
      @top_ten_high_score_list = Game.all.sort_by(&:score).reverse.slice!(0..9)
    end
    if current_gamer
      @top_ten_user_high_score_list = Game.find(:all, :from => :find_by_gamer_id, 
        :params => {:gamer_id => current_gamer.id}).sort_by(&:score).reverse.slice!(0..9)
    end
  end
end
