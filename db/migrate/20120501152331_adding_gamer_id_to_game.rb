class AddingGamerIdToGame < ActiveRecord::Migration
  def up
    change_table :gamers do |t|
      t.integer :game_id
    end
  end

  def down
  end
end
